load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_VERSION = "0.8.2"

def websocketpp():
    maybe(
        http_archive,
        name = "websocketpp",
        sha256 = "6ce889d85ecdc2d8fa07408d6787e7352510750daa66b5ad44aacb47bea76755",
        build_file = Label("//:third_party/websocketpp/websocketpp.BUILD"),
        url = "https://github.com/zaphoyd/websocketpp/archive/refs/tags/{version}.tar.gz".format(version = _VERSION),
        strip_prefix = "websocketpp-{version}".format(version = _VERSION),
    )
