/*******************************************************************************
 * Copyright (c) 2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/GroundTruth/Internal/Static/stationary_object_proto_converter_utils.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <gtest/gtest.h>

namespace gtgen::core::environment::proto_groundtruth
{
using ProtoStationaryObjectClassification = osi3::StationaryObject::Classification;

class StationaryObjectTypeConverterTestFixture
    : public testing::TestWithParam<std::tuple<mantle_api::StaticObjectType, ProtoStationaryObjectClassification::Type>>
{
};

INSTANTIATE_TEST_SUITE_P(
    StationaryObjectConverterUtils,
    StationaryObjectTypeConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<mantle_api::StaticObjectType, ProtoStationaryObjectClassification::Type>>{
        std::make_tuple(mantle_api::StaticObjectType::kOther, ProtoStationaryObjectClassification::TYPE_OTHER),
        std::make_tuple(mantle_api::StaticObjectType::kBridge, ProtoStationaryObjectClassification::TYPE_BRIDGE),
        std::make_tuple(mantle_api::StaticObjectType::kBuilding, ProtoStationaryObjectClassification::TYPE_BUILDING),
        std::make_tuple(mantle_api::StaticObjectType::kPole, ProtoStationaryObjectClassification::TYPE_POLE),
        std::make_tuple(mantle_api::StaticObjectType::kPylon, ProtoStationaryObjectClassification::TYPE_PYLON),
        std::make_tuple(mantle_api::StaticObjectType::kDelineator,
                        ProtoStationaryObjectClassification::TYPE_DELINEATOR),
        std::make_tuple(mantle_api::StaticObjectType::kTree, ProtoStationaryObjectClassification::TYPE_TREE),
        std::make_tuple(mantle_api::StaticObjectType::kBarrier, ProtoStationaryObjectClassification::TYPE_BARRIER),
        std::make_tuple(mantle_api::StaticObjectType::kVegetation,
                        ProtoStationaryObjectClassification::TYPE_VEGETATION),
        std::make_tuple(mantle_api::StaticObjectType::kCurbstone, ProtoStationaryObjectClassification::TYPE_CURBSTONE),
        std::make_tuple(mantle_api::StaticObjectType::kWall, ProtoStationaryObjectClassification::TYPE_WALL),
        std::make_tuple(mantle_api::StaticObjectType::kVerticalStructure,
                        ProtoStationaryObjectClassification::TYPE_VERTICAL_STRUCTURE),
        std::make_tuple(mantle_api::StaticObjectType::kRectangularStructure,
                        ProtoStationaryObjectClassification::TYPE_RECTANGULAR_STRUCTURE),
        std::make_tuple(mantle_api::StaticObjectType::kOverheadStructure,
                        ProtoStationaryObjectClassification::TYPE_OVERHEAD_STRUCTURE),
        std::make_tuple(mantle_api::StaticObjectType::kReflectiveStructure,
                        ProtoStationaryObjectClassification::TYPE_REFLECTIVE_STRUCTURE),
        std::make_tuple(mantle_api::StaticObjectType::kConstructionSiteElement,
                        ProtoStationaryObjectClassification::TYPE_CONSTRUCTION_SITE_ELEMENT),
        std::make_tuple(mantle_api::StaticObjectType::kSpeedBump, ProtoStationaryObjectClassification::TYPE_SPEED_BUMP),
        std::make_tuple(mantle_api::StaticObjectType::kEmittingStructure,
                        ProtoStationaryObjectClassification::TYPE_EMITTING_STRUCTURE)}));

TEST_P(StationaryObjectTypeConverterTestFixture, GivenStaticObjectType_WhenConvertToProtoType_ThenCorrectTypeReturned)
{
    mantle_api::StaticObjectType gtgen_type = std::get<0>(GetParam());
    ProtoStationaryObjectClassification::Type expected_proto_type = std::get<1>(GetParam());

    auto actual_proto_type = ConvertStaticObjectTypeToProtoStationaryObjectType(gtgen_type);

    EXPECT_EQ(expected_proto_type, actual_proto_type);
}

}  // namespace gtgen::core::environment::proto_groundtruth
