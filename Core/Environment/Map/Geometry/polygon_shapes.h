/*******************************************************************************
 * Copyright (c) 2020-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2020-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POLYGONSHAPES_H
#define GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POLYGONSHAPES_H

#include "glm/vec2.hpp"

#include <vector>

/// @brief
///    ________
///   |        |
///   |        |
///   |________|
///
// NOLINTNEXTLINE(cert-err58-cpp)
const static std::vector<glm::dvec2> kSquare = {{1.0, 0.0}, {0.0, 0.0}, {0.0, 1.0}, {1.0, 1.0}};

/// @brief
///    ___  ___
///   |   \/   |
///   |        |
///   |________|
///
// NOLINTNEXTLINE(cert-err58-cpp)
const static std::vector<glm::dvec2> kSquareWithInnerDent = {{0.0, 1.0},
                                                             {0.5, 0.5},
                                                             {1.0, 1.0},
                                                             {1.0, 0.0},
                                                             {0.0, 0.0}};

/// @brief
///       .
///      .   .
///     .      .
///    .      .
///      .   .
///         .
///
// NOLINTNEXTLINE(cert-err58-cpp)
const static std::vector<glm::dvec2> kRotatedSquare = {{2.0, 0.0}, {0.0, 1.0}, {1.0, 3.0}, {3.0, 2.0}};

/// @brief
///       ____
///      |    |
///  ____|    |____
/// |              |
/// |____      ____|
///      |    |
///      |____|
///
// NOLINTNEXTLINE(cert-err58-cpp)
const static std::vector<glm::dvec2> kGreekCross = {{0.0, 1.0},
                                                    {0.0, 2.0},
                                                    {1.0, 2.0},
                                                    {1.0, 3.0},
                                                    {2.0, 3.0},
                                                    {2.0, 2.0},
                                                    {3.0, 2.0},
                                                    {3.0, 1.0},
                                                    {2.0, 1.0},
                                                    {2.0, 0.0},
                                                    {1.0, 0.0},
                                                    {1.0, 1.0}};

#endif  // GTGEN_CORE_ENVIRONMENT_MAP_GEOMETRY_POLYGONSHAPES_H
