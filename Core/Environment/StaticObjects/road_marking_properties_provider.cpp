/*******************************************************************************
 * Copyright (c) 2024, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Core/Environment/StaticObjects/road_marking_properties_provider.h"

#include "Core/Environment/StaticObjects/utils.h"
#include "Core/Service/Logging/logging.h"

namespace gtgen::core::environment::static_objects
{
using units::literals::operator""_m;

std::unique_ptr<mantle_api::StaticObjectProperties> RoadMarkingPropertiesProvider::Get(const std::string& identifier)
{
    if (identifier.empty())
    {
        return nullptr;
    }

    auto road_marking_properties = std::make_unique<mantle_ext::RoadMarkingProperties>();
    road_marking_properties->type = mantle_api::EntityType::kStatic;
    road_marking_properties->bounding_box.dimension = {0.0_m, 0.0_m, 0.0_m};

    road_marking_properties->marking_color = osi::OsiRoadMarkingColor::kWhite;
    road_marking_properties->marking_type = GetOsiMarkingType(identifier);
    road_marking_properties->sign_type = GetOsiSignType(identifier);
    road_marking_properties->text = "";
    road_marking_properties->value = GetSignalValue(identifier);
    road_marking_properties->unit = GetSignValueUnit(identifier);

    road_marking_properties->vertical_offset = 0.0_m;

    return road_marking_properties;
}

osi::OsiRoadMarkingsType RoadMarkingPropertiesProvider::GetOsiMarkingType(const std::string& identifier)
{
    if (auto it = kStvoSignsToOsiTypes.find(identifier); it != kStvoSignsToOsiTypes.end())
    {
        return it->second.first;
    }

    std::string type_identifier = GetTypeIdentifier(identifier);
    if (auto it = kStvoSignsToOsiTypes.find(type_identifier); it != kStvoSignsToOsiTypes.end())
    {
        return it->second.first;
    }

    Warn("Could not convert provided identifier '{}' to OSI road marking type. It will be treated as TYPE_OTHER.",
         identifier);

    return osi::OsiRoadMarkingsType::kOther;
}

osi::OsiTrafficSignType RoadMarkingPropertiesProvider::GetOsiSignType(const std::string& identifier)
{
    if (auto it = kStvoSignsToOsiTypes.find(identifier); it != kStvoSignsToOsiTypes.end())
    {
        return it->second.second;
    }

    std::string type_identifier = GetTypeIdentifier(identifier);
    if (auto it = kStvoSignsToOsiTypes.find(type_identifier); it != kStvoSignsToOsiTypes.end())
    {
        return it->second.second;
    }

    Warn("Could not convert provided identifier '{}' to OSI sign type. It will be treated as TYPE_OTHER.", identifier);

    return osi::OsiTrafficSignType::kOther;
}

double RoadMarkingPropertiesProvider::GetSignalValue(const std::string& identifier)
{
    // maps german traffic law (StVO) sign labels to values they are showing
    // when their subtype does not reflect the value provided as type-subtype
    /// @todo: currently no known list for road marking signal value, to be filled
    const static std::map<std::string, double> special_stvo_sign_to_value = {};

    // Check the list of known values for certain special signs
    if (auto it = special_stvo_sign_to_value.find(identifier); it != special_stvo_sign_to_value.end())
    {
        return it->second;
    }

    // Now, try to derive the value from the subtype, because this is how most StVO signs work.
    // This is a very limited assumption, so we only convert if it's very clear. That means e.g. that
    // for now we only convert integers, so 314-50 will work, but 314-50.5 won't
    auto sub_type = GetSignalSubType(identifier);
    if (sub_type.has_value())
    {
        try
        {
            return std::stoi(sub_type.value());
        }
        catch (const std::invalid_argument& e)
        {
            Warn(
                "No road marking sign value information could be extracted from the provided identifier '{}'.The given "
                "identifier could not be translated into a number ",
                identifier);
        }
    }

    Warn("No road marking sign value information could be extracted from the provided identifier '{}'.", identifier);
    return -1;
}

osi::OsiTrafficSignValueUnit RoadMarkingPropertiesProvider::GetSignValueUnit(const std::string& identifier)
{
    /// @todo: currently no known unit for road marking unit, to be filled
    const static std::map<std::string, osi::OsiTrafficSignValueUnit> special_stvo_sign_to_unit = {};

    std::string type_identifier = GetTypeIdentifier(identifier);
    if (auto it = special_stvo_sign_to_unit.find(type_identifier); it != special_stvo_sign_to_unit.end())
    {
        return it->second;
    }

    Warn("Could not convert provided identifier '{}' to OSI sign value unit. It will be treated as TYPE_UNKNOWN.",
         identifier);

    return osi::OsiTrafficSignValueUnit::kUnknown;
}

// maps german traffic law (StVO) sign labels to OSI road marking types & OSI signs
// NOLINTNEXTLINE(cert-err58-cpp)
const RoadMarkingPropertiesProvider::DataMap RoadMarkingPropertiesProvider::kStvoSignsToOsiTypes{
    {"293", {osi::OsiRoadMarkingsType::kSymbolicTrafficSign, osi::OsiTrafficSignType::kZebraCrossing}},
    {"294", {osi::OsiRoadMarkingsType::kSymbolicTrafficSign, osi::OsiTrafficSignType::kStop}},
    {"299", {osi::OsiRoadMarkingsType::kSymbolicTrafficSign, osi::OsiTrafficSignType::kNoParking}},
    {"341", {osi::OsiRoadMarkingsType::kSymbolicTrafficSign, osi::OsiTrafficSignType::kGiveWay}},
};

}  // namespace gtgen::core::environment::static_objects
