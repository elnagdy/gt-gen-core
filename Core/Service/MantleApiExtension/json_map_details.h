/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2021-2023, Ansys, Inc.
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/
#ifndef GTGEN_CORE_SERVICE_MANTLEAPIEXTENSION_JSONMAPDETAILS_H
#define GTGEN_CORE_SERVICE_MANTLEAPIEXTENSION_JSONMAPDETAILS_H

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Map/map_details.h>

#include <array>
#include <memory>
#include <vector>

namespace mantle_ext
{

struct FrictionPatch
{
    std::string road{};
    std::int32_t lane{0};

    std::array<units::length::meter_t, 2> s_offset{};
    std::array<units::length::meter_t, 2> t_offset{};

    double friction{0};
};

struct JsonMapDetails : public mantle_api::MapDetails
{
    std::vector<FrictionPatch> friction_patches;

    virtual std::unique_ptr<mantle_api::MapDetails> Clone() const override
    {
        return std::make_unique<JsonMapDetails>(*this);
    }
};

}  // namespace mantle_ext

#endif  // GTGEN_CORE_SERVICE_MANTLEAPIEXTENSION_JSONMAPDETAILS_H
