"""
Rule to provide a sanity check for artifactory URL.
"""

def get_url(repo, path):
    """Macro to provide a debian package hosted on artifactory as an external project.

    Args:
        repo: The url to the artifactory repository.
        path: The url path to the file inside the artifactory repository.

    Returns:
      Valid url
    """

    if repo.endswith("/") and path.startswith("/"):
        return repo.rstrip("/") + path
    elif repo.endswith("/") or path.startswith("/"):
        return repo + path
    else:
        return repo + "/" + path
